﻿/********************************************************************************************************************
 * COMPANY  : B BRAUN
 * SUBJECT  : TEST
 * CLASS    : MAIN CLASS
 * DATE     : MAY 13TH 2020
 * AUTHOR   : OSCAR ROLANDO ALVAREZ
 * EMAIL    : oralvarez@gmail.com
 * *****************************************************************************************************************/

using System;
using System.Collections.Generic;

namespace Graphos
{

    class Program
    {

        static void Main(string[] args)
        {
            // CHALLENGE NUMBER 1: GRAPH
            int [,] pairs = {
                {10, 3}, {2, 3}, {3, 6}, {5, 6}, {5, 17},
                {4, 5}, {4, 8}, {8, 9}, {3, 1}
            };

            GraphChallenge.PrintGraphChallenge(pairs);

            CalculatorClass.CalculateOnConsole("1+2+3+4+5");
        }
    }
}
