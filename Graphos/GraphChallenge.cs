﻿/********************************************************************************************************************
 * COMPANY  : B BRAUN
 * SUBJECT  : TEST
 * CLASS    : GRAPHOS CLASS
 * DATE     : MAY 13TH 2020
 * AUTHOR   : OSCAR ROLANDO ALVAREZ
 * EMAIL    : oralvarez@gmail.com
 * *****************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Graphos
{
    public class NodeInt
    {
        public int id;
        public int parent;
        public int level;
        public int totalLeafs;

        public NodeInt(int id, int parent)
        {
            this.id = id;
            this.parent = parent;
        }
        public NodeInt(int id, int parent, int level)
        {
            this.id = id;
            this.parent = parent;
            this.level = level;
        }
    }
    public static class GraphChallenge
    {
        public static void PrintGraphChallenge(int[,] arr)
        {
            int parentId = 0;
            int nodeId = 0;

            List<NodeInt> TotalTree = new List<NodeInt>();
            List<NodeInt> ParentsTree = new List<NodeInt>();

            // 001. ---- POPULATING TOTAL TREE ---
            System.Console.WriteLine("001. ---- POPULATING TOTAL TREE ---");

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                NodeInt nodeTotal = new NodeInt(arr[i, 1], arr[i, 0]);
                NodeInt nodeParent = new NodeInt(nodeTotal.parent, 0, 0);
                TotalTree.Add(nodeTotal);
                ParentsTree.Add(nodeParent);

            }

            // 002. ---- UPDATE PARENTS TREE ---
            System.Console.WriteLine("002. ---- UPDATE PARENTS TREE ---");
            foreach (NodeInt nodeParentInt in ParentsTree)
            {
                nodeParentInt.level = 0;

                foreach (NodeInt nodeInt in TotalTree)
                {
                    if (nodeInt.id == nodeParentInt.id)
                    {
                        nodeParentInt.level = 1;
                    }
                }
            }

            /*
             * 003. SHOW TOTAL TREE
             * */
            System.Console.WriteLine("003. ---- SHOW TOTAL TREE ---");
            foreach (NodeInt nodeInt in TotalTree)
            {
                System.Console.WriteLine(string.Format("PARENT = {0}, ID = {1}", nodeInt.parent, nodeInt.id));
            }

            /*
             * 004. SHOW ONLY TREE WITH PARENTS ZERO
             * */
            System.Console.WriteLine("004. ---- SHOW PARENTS WITH LEVEL 0 ---");
            List<NodeInt> filteredParents = ParentsTree.FindAll(item => item.level == 0);
            foreach (NodeInt NodeInt in filteredParents)
            {
                if (parentId != NodeInt.id)
                    System.Console.WriteLine(string.Format("PARENTS LEVEL 0: {0}", NodeInt.id));

                parentId = NodeInt.id;
            }

            /*
             * 005. SHOW VALUE WITH SINGLE PARENT 
             * */
            System.Console.WriteLine("005. ---- SHOW VALE WITH SINGLE PARENT ---");
            parentId = 0;
            nodeId = 0;
            int totalLeafs = 0;

            foreach (NodeInt nodeIntA in TotalTree)
            {
                nodeId = nodeIntA.id;
                totalLeafs = 0;
                foreach (NodeInt nodeIntB in TotalTree.FindAll(item => item.id == nodeId))
                {
                    totalLeafs++; 
                }
                nodeIntA.totalLeafs = totalLeafs;
            }

            List<NodeInt> filteredLeafs = TotalTree.FindAll(item => item.totalLeafs == 1);
            foreach (NodeInt NodeInt in filteredLeafs)
            {
                System.Console.WriteLine(string.Format("PARENTS LEVEL 0: {0}", NodeInt.id));
            }

        }
    }
}
